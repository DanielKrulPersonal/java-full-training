create table if not exists engines
(
    uuid  varchar(36) not null,
    size  double      not null,
    power double      not null,
    price double      not null,
    constraint engines_uuid_uindex
        unique (uuid)
);

alter table engines
    add primary key (uuid);

create table if not exists transmissions
(
    uuid  varchar(36)             not null,
    type  enum ('AUTO', 'MANUAL') not null,
    price double                  not null,
    constraint transmissions_uuid_uindex
        unique (uuid)
);

alter table transmissions
    add primary key (uuid);

create table if not exists wheels
(
    uuid  varchar(36) not null,
    size  double      not null,
    color varchar(30) not null,
    style varchar(30) not null,
    price double      not null,
    constraint wheels_uuid_uindex
        unique (uuid)
);

alter table wheels
    add primary key (uuid);


create table if not exists cars
(
    uuid          varchar(36) not null,
    paintColor    varchar(30) not null,
    numberOfDoors int         not null,
    engine        varchar(36) not null,
    transmission  varchar(36) not null,
    flWheel       varchar(36) not null,
    frWheel       varchar(36) not null,
    rlWheel       varchar(36) not null,
    rrWheel       varchar(36) not null,
    constraint cars_uuid_uindex
        unique (uuid)
);

alter table cars
    add primary key (uuid);

