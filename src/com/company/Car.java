package com.company;

import com.company.observables.CarDoorsObservable;
import com.company.observables.CarEngineObservable;
import com.company.parts.Engine;
import com.company.parts.Transmission;
import com.company.parts.TransmissionType;
import com.company.parts.Wheel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Observable;
import java.util.Observer;

public class Car implements Observer {
    private String paintColor;
    private Engine engineInstance;
    private Transmission transmissionInstance;
    private Wheel frontLeftWheelInstance;
    private Wheel frontRightWheelInstance;
    private Wheel rearLeftWheelInstance;
    private Wheel rearRightWheelInstance;
    private String id;
    private int numberOfDoors;
    private CarDoorsObservable numberOfDoorsObservable;
    private CarEngineObservable carEngineObservable;

    private Car() {
    }

    public static Car builder() {
        Car car = new Car();
        car.id = Utils.generateId();
        return car;
    }

    public Car build() {
        return this;
    }

    //setters
    public void setEngine(Engine engineInstance) {
        this.engineInstance = engineInstance;
    }

    public void setTransmission(Transmission transmissionInstance) {
        this.transmissionInstance = transmissionInstance;
    }

    public void setWheels(double price, double size, String color, String style) {
        frontLeftWheelInstance = new Wheel(price, size, color, style);
        frontRightWheelInstance = new Wheel(price, size, color, style);
        rearLeftWheelInstance = new Wheel(price, size, color, style);
        rearRightWheelInstance = new Wheel(price, size, color, style);
    }

    public void setNumberOfDoors(int numberOfDoors) {
        this.numberOfDoors = numberOfDoors;
    }

    public void setPaintColor(String paintColor) {
        this.paintColor = paintColor;
    }

    // getters
    public String getPaintColor() {
        return paintColor;
    }

    public double getEnginePower() {
        return engineInstance.getPower();
    }

    public String getId() {
        return id;
    }

    public double getCost() {
        double totalPrice = engineInstance.getPrice() + transmissionInstance.getPrice();
        totalPrice += frontLeftWheelInstance.getPrice() + frontRightWheelInstance.getPrice();
        totalPrice += rearLeftWheelInstance.getPrice() + rearRightWheelInstance.getPrice();
        return totalPrice;
    }

    public TransmissionType getTransmissionType() {
        return this.transmissionInstance.getType();
    }

    public String getWheelsStyle() {
        return this.frontLeftWheelInstance.getStyle();
    }

    public int getNumberOfDoors() {
        return numberOfDoors;
    }

    public Engine getEngine() {
        return engineInstance;
    }

    public Transmission getTransmission() {
        return transmissionInstance;
    }

    public void openDoors() {
        numberOfDoorsObservable.openDoors(this);
    }

    public void startEngine() {
        carEngineObservable.startEngine(this.engineInstance);
    }

    public void update(Observable obj, Object arg) {
        System.out.println(arg);
    }

    // initializers of observables
    public void initializeNumberOfDoorsObservable() {
        this.numberOfDoorsObservable = new CarDoorsObservable();
        numberOfDoorsObservable.addObserver(this);
    }

    public void initializeCarEngineObservable() {
        this.carEngineObservable = new CarEngineObservable();
        carEngineObservable.addObserver(this);
    }

    // database
    public void storeInDatabase() {
        Connection connection = DatabaseConnection.getInstance().getConnection();

        String carInsert = "INSERT INTO cars(" +
                "uuid, paintColor, numberOfDoors, engine, transmission, flWheel, frWheel, rlWheel, rrWheel" +
                ") VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try {
            PreparedStatement carInsertPst = connection.prepareStatement(carInsert);

            carInsertPst.setString(1, getId());
            carInsertPst.setString(2, getPaintColor());
            carInsertPst.setInt(3, numberOfDoors);
            carInsertPst.setString(4, engineInstance.getId());
            carInsertPst.setString(5, transmissionInstance.getId());
            carInsertPst.setString(6, frontLeftWheelInstance.getId());
            carInsertPst.setString(7, frontRightWheelInstance.getId());
            carInsertPst.setString(8, rearLeftWheelInstance.getId());
            carInsertPst.setString(9, rearRightWheelInstance.getId());
            carInsertPst.executeUpdate();
        } catch (SQLException e) {
            System.err.println();
        }
    }
}
