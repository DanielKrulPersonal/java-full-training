package com.company;

import com.company.parts.Engine;
import com.company.parts.Transmission;
import com.company.parts.TransmissionType;

public class CarFactory {

    public static Car getCar(CarSpecification carSpecification, CarType type){
        Engine engine;
        Transmission transmission;

        Car carInstance = Car.builder().build();
        double wheelPrice;
        double wheelSize;

        switch (type) {
            case SEDAN:
                engine = new Engine(1250.0, 2100.0, 155);
                transmission = new Transmission(200.0, TransmissionType.MANUAL);
                wheelPrice = 100;
                wheelSize = 17;
                carInstance.setNumberOfDoors(4);
                break;
            case AVANT:
                engine = new Engine(999.0, 1500.0, 89);
                transmission = new Transmission(150.0, TransmissionType.AUTO);
                wheelPrice = 70;
                wheelSize = 15;
                carInstance.setNumberOfDoors(5);
                break;

            case CUSTOM:
                engine = new Engine(1250.0, 2100.0, 155);
                transmission = new Transmission(200.0, carSpecification.getTransmissionType());
                wheelPrice = 120;
                wheelSize = carSpecification.getWheelSize();
                carInstance.setNumberOfDoors(4);
                break;

            default:
                throw new IllegalStateException("Unexpected value: " + type);
        }

        carInstance.setEngine(engine);
        carInstance.setTransmission(transmission);
        carInstance.setWheels(wheelPrice, wheelSize, carSpecification.getWheelColor(), carSpecification.getWheelStyle());
        carInstance.setPaintColor(carSpecification.getPaintColor());
        carInstance.storeInDatabase();
        carInstance.getEngine().storeInDatabase();
        carInstance.getTransmission().storeInDatabase();

        // set observables
        carInstance.initializeNumberOfDoorsObservable();
        carInstance.initializeCarEngineObservable();
        return carInstance;
    }
}