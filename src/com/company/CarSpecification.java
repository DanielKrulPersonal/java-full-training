package com.company;

import com.company.parts.TransmissionType;

public class CarSpecification {
    private double wheelSize;
    private String paintColor = "black";
    private TransmissionType transmissionType;
    private String wheelColor = "silver";
    private String wheelStyle = "concave";

    public static CarSpecification builder() {
        return new CarSpecification();
    }

    public CarSpecification build() {
        return this;
    }

    // setters
    public CarSpecification withWheelSize(double wheelSize) {
        this.wheelSize = wheelSize;
        return this;
    }

    public CarSpecification withPaintColor(String paintColor) {
        this.paintColor = paintColor;
        return this;
    }

    public CarSpecification withTransmissionType(TransmissionType transmissionType) {
        this.transmissionType = transmissionType;
        return this;
    }

    public CarSpecification withWheelColor(String wheelColor) {
        this.wheelColor = wheelColor;
        return this;
    }

    public CarSpecification withWheelStyle(String wheelStyle) {
        this.wheelStyle = wheelStyle;
        return this;
    }

    // getters
    public String getPaintColor() {
        return paintColor;
    }

    public TransmissionType getTransmissionType() {
        return transmissionType;
    }

    public double getWheelSize() {
        return wheelSize;
    }

    public String getWheelColor() {
        return wheelColor;
    }

    public String getWheelStyle() {
        return wheelStyle;
    }
}
