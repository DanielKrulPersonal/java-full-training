package com.company;

public enum CarType {
    SEDAN,
    AVANT,
    CUSTOM
}
