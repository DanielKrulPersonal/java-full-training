package com.company;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {
    private static final String USERNAME = "training_user";
    private static final String PASSWORD = "training_pass";
    private static final String CONN_STRING = "jdbc:mysql://localhost:6666/training";
    private Connection connection;

    private static DatabaseConnection databaseConnectionInstance = null;

    private DatabaseConnection() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(CONN_STRING, USERNAME, PASSWORD);
        } catch (SQLException | ClassNotFoundException e) {
            System.err.println(e);
        }
    }

    // static method to create instance of Singleton class
    public static DatabaseConnection getInstance()
    {
        if (databaseConnectionInstance == null)
            databaseConnectionInstance = new DatabaseConnection();

        return databaseConnectionInstance;
    }

    public Connection getConnection() {
        return connection;
    }
}
