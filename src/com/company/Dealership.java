package com.company;

public class Dealership {
    public Car orderSportsCar() {
        return CarFactory.getCar(CarSpecification.builder().build(), CarType.SEDAN);
    }

    public Car getFamilyCar() {
        return CarFactory.getCar(CarSpecification.builder().build(), CarType.AVANT);
    }

    public Car orderCustomCar(CarSpecification carSpecification) {
        return CarFactory.getCar(carSpecification, CarType.CUSTOM);
    }
}
