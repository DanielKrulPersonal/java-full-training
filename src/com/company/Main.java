package com.company;

import com.company.parts.TransmissionType;

public class Main {
    public static void main(String[] args) {
        Dealership dealership = new Dealership();
        Car sportsCar = dealership.orderSportsCar();
        sportsCar.openDoors();
        sportsCar.startEngine();

        Car familyCar = dealership.getFamilyCar();
        familyCar.openDoors();
        familyCar.startEngine();

        CarSpecification carSpecification = CarSpecification.builder()
                    .withWheelSize(20)
                    .withPaintColor("red")
                    .withTransmissionType(TransmissionType.AUTO)
                    .build();

        Car customCar = dealership.orderCustomCar(carSpecification);
        System.out.printf("The custom car ordered costs: %s\n", customCar.getCost());
        customCar.openDoors();
        customCar.startEngine();
    }
}
