package com.company.observables;

import com.company.Car;

import java.util.Observable;

public class CarDoorsObservable extends Observable {
    public void openDoors(Car car) {
        String message = car.getNumberOfDoors() + " doors were opened";
        setChanged();
        notifyObservers(message);
    }
}
