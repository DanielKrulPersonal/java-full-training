package com.company.observables;

import com.company.parts.Engine;

import java.util.Observable;

public class CarEngineObservable extends Observable {
    public void startEngine(Engine engine) {
        String message = (engine.isNoisy() ? "A noisy" : "A silent") + " engine was started";
        setChanged();
        notifyObservers(message);
    }
}
