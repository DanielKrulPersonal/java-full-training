package com.company.parts;

import com.company.DatabaseConnection;
import com.company.Utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Engine extends Part {
    /**
     * cm3
     */
    private final double size;
    /**
     * horsepower
     */
    private final double power;

    public Engine(double price, double size, double power) {
        this.setPrice(price);
        this.size = size;
        this.power = power;
        this.setId(Utils.generateId());
    }

    // getters
    public double getSize() {
        return size;
    }

    public double getPower() {
        return power;
    }

    public boolean isNoisy() {
        return getPower() > 150 && getSize() > 2000;
    }

    // database
    public void storeInDatabase() {
        Connection connection = DatabaseConnection.getInstance().getConnection();

        String engineInsert = "INSERT INTO engines(" +
                "uuid, size, power, price" +
                ") VALUES(?, ?, ?, ?)";

        try {
            PreparedStatement engineInsertPst = connection.prepareStatement(engineInsert);

            engineInsertPst.setString(1, getId());
            engineInsertPst.setDouble(2, getSize());
            engineInsertPst.setDouble(3, getPower());
            engineInsertPst.setDouble(4, getPrice());
            engineInsertPst.executeUpdate();
        } catch (SQLException e) {
            System.err.println();
        }
    }
}
