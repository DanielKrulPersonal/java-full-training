package com.company.parts;

abstract class Part {
    private String id;
    private double price;

    // getters
    public String getId() {
        return id;
    }

    public double getPrice() {
        return price;
    }

    // setters
    public void setId(String id) {
        this.id = id;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
