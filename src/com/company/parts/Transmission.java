package com.company.parts;

import com.company.DatabaseConnection;
import com.company.Utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Transmission extends Part {
    private final TransmissionType type;

    public Transmission(double price, TransmissionType type) {
        this.setPrice(price);
        this.type = type;
        this.setId(Utils.generateId());
    }

    public TransmissionType getType() {
        return type;
    }

    // database
    public void storeInDatabase() {
        Connection connection = DatabaseConnection.getInstance().getConnection();

        String transmissionInsert = "INSERT INTO transmissions(" +
                "uuid, type, price" +
                ") VALUES(?, ?, ?)";

        try {
            PreparedStatement transmissionInsertPst = connection.prepareStatement(transmissionInsert);

            transmissionInsertPst.setString(1, getId());
            transmissionInsertPst.setString(2, getType().toString());
            transmissionInsertPst.setDouble(3, getPrice());
            transmissionInsertPst.executeUpdate();
        } catch (SQLException e) {
            System.err.println();
        }
    }
}
