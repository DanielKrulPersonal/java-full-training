package com.company.parts;

import com.company.Utils;

public class Wheel extends Part {
    /**
     * In inches
     */
    private final double size;
    private final String color;
    private final String style;

    public Wheel(double price, double size, String color, String style) {
        this.setPrice(price);
        this.size = size;
        this.color = color;
        this.style = style;
        this.setId(Utils.generateId());
    }

    public double getSize() {
        return size;
    }

    public String getColor() {
        return color;
    }

    public String getStyle() {
        return style;
    }
}
